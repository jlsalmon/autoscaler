package main

import (
	"context"
	"os"

	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/cmd/autoscaler/commands/custom"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/cmd/autoscaler/commands/selftest"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/config"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/executors"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/executors/ssh"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/executors/winrm"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/internal/cli"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/internal/logging"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/internal/logging/storage"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/internal/runner"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/internal/signal"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/providers"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/providers/aws"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/providers/gcp"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/providers/orka"
)

const (
	defaultConfigFile = "config.toml"
)

type globalFlags struct {
	Debug     bool   `long:"debug" description:"Set debug log level"`
	LogLevel  string `long:"log-level" description:"Set custom log level (debug, info, warning, error, fatal, panic)"`
	LogFile   string `long:"log-file" description:"File where logs should be saved"`
	LogFormat string `long:"log-format" description:"Format of log (text, json)"`

	ConfigFile string `long:"config" description:"Path to configuration file"`
}

var (
	global = &globalFlags{
		ConfigFile: defaultConfigFile,
	}

	closeLogFile = cli.NewNopHook()
)

func main() {
	logger := logging.New()

	registerFactories()
	ctx := startSignalHandler(logger)

	a := setUpApplication(ctx, logger)

	err := a.Run(os.Args)
	if err != nil {
		logger.
			WithError(err).
			Error("Application execution failed")

		runner.GetAdapter().GenerateExitFromError(err)
	}
}

func registerFactories() {
	providers.MustRegister(aws.Name, aws.New)
	providers.MustRegister(gcp.Name, gcp.New)
	providers.MustRegister(orka.Name, orka.New)

	executors.MustRegister(winrm.Name, winrm.OS, winrm.New)
	executors.MustRegister(ssh.Name, ssh.OS, ssh.New)
}

func startSignalHandler(logger logging.Logger) context.Context {
	terminationHandler := signal.NewTerminationHandler(logger)
	go terminationHandler.HandleSignals()

	return terminationHandler.Context()
}

func setUpApplication(ctx context.Context, logger logging.Logger) *cli.App {
	a := cli.New(ctx, autoscaler.NAME, "Autoscaling driver for GitLab Runner")

	a.AddBeforeFunc(func(ctx *cli.Context) error {
		ctx.SetLogger(logger)

		return nil
	})
	a.AddBeforeFunc(loadConfigurationFile)
	a.AddBeforeFunc(updateLogLevel)
	a.AddBeforeFunc(updateLogFormat)
	a.AddBeforeFunc(setLoggingToFile)
	a.AddBeforeFunc(logStartupMessage)

	// If logging to file will be set, closeLogFile() will close the
	// used file. Otherwise tis a NOP call.
	a.AddAfterFunc(closeLogFile)

	a.AddGlobalFlagsFromStruct(global)

	a.RegisterCategory(custom.NewCustomCategory())
	a.RegisterCategory(selftest.NewSelftTestCategory())

	return a
}

func logStartupMessage(ctx *cli.Context) error {
	ctx.
		Logger().
		WithFields(logging.Fields{
			"version": autoscaler.Version().ShortLine(),
		}).
		Infof("Starting %s", autoscaler.NAME)

	return nil
}

func loadConfigurationFile(ctx *cli.Context) error {
	cfg, err := config.LoadFromFile(global.ConfigFile)
	if err != nil {
		return err
	}

	ctx.SetConfig(cfg)

	return nil
}

func updateLogLevel(ctx *cli.Context) error {
	logLevel := ctx.Config().LogLevel
	if global.Debug {
		logLevel = "debug"
	} else if global.LogLevel != "" {
		logLevel = global.LogLevel
	}

	if logLevel == "" {
		return nil
	}

	return ctx.Logger().SetLevel(logLevel)
}

func updateLogFormat(ctx *cli.Context) error {
	logFormat := ctx.Config().LogFormat
	if global.LogFormat != "" {
		logFormat = global.LogFormat
	}

	if logFormat == "" {
		return nil
	}

	return ctx.Logger().SetFormat(logFormat)
}

func setLoggingToFile(ctx *cli.Context) error {
	logFile := ctx.Config().LogFile
	if global.LogFile != "" {
		logFile = global.LogFile
	}

	if logFile == "" {
		return nil
	}

	logStorage := storage.NewFile(logFile)
	err := logStorage.Open()
	if err != nil {
		return err
	}

	closeLogFile = func(ctx *cli.Context) error {
		return logStorage.Close()
	}

	ctx.Logger().SetOutput(logStorage)

	return nil
}
