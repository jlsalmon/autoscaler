package executors

import (
	"context"
	"fmt"
	"io"

	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/config"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/internal/factories"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/internal/logging"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/vm"
)

const factoryType = "executor"

var factoriesRegistry = factories.NewRegistry(factoryType)

// maps which executor to use for which OS.
var executorForOS = map[string]string{}

type OSExecutorMissingError struct {
	os string
}

func (e *OSExecutorMissingError) Error() string {
	return fmt.Sprintf("no executor found for OS %q", e.os)
}

func (e *OSExecutorMissingError) Is(err error) bool {
	_, ok := err.(*OSExecutorMissingError)
	return ok
}

type Executor interface {
	Execute(ctx context.Context, vmInst vm.Instance, script []byte) error
	ExecuteWithWriters(ctx context.Context, vmInst vm.Instance, script []byte, stdout, stderr io.Writer) error
	ProvisionScript() string
}

type Factory func(cfg config.Global, logger logging.Logger) (Executor, error)

func MustRegister(name string, os string, f Factory) {
	executorForOS[os] = name

	factoriesRegistry.MustRegister(name, f)
}

func Factorize(cfg config.Global, logger logging.Logger) (Executor, error) {
	executor, ok := executorForOS[cfg.OS]
	if !ok {
		return nil, &OSExecutorMissingError{os: cfg.OS}
	}

	factory, err := factoriesRegistry.Get(executor)
	if err != nil {
		return nil, err
	}

	return factory.(Factory)(cfg, logger)
}
