package ssh

import (
	"context"
	"fmt"
	"io"
	"os"

	"golang.org/x/crypto/ssh"

	globalConfig "gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/config"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/executors"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/internal/dial"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/internal/logging"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/vm"
)

const (
	Name = "ssh"
	OS   = "macos"
)

type Executor struct {
	logger logging.Logger
}

func New(cfg globalConfig.Global, logger logging.Logger) (executors.Executor, error) {
	return &Executor{
		logger: logger,
	}, nil
}

func (e *Executor) ExecuteWithWriters(ctx context.Context, vmInst vm.Instance, script []byte, stdout, stderr io.Writer) error {
	return nil
}

func (e *Executor) Execute(ctx context.Context, vmInst vm.Instance, script []byte) error {
	authMethod, err := authMethod(vmInst)
	if err != nil {
		return fmt.Errorf("authmethod invalid: %w", err)
	}

	config := &ssh.ClientConfig{
		User:            vmInst.Username,
		HostKeyCallback: ssh.InsecureIgnoreHostKey(),
		Auth:            []ssh.AuthMethod{authMethod},
		Timeout:         dial.DefaultSSHTimeout,
	}

	client, err := dial.SSHWithDeadline(ctx, "tcp", vmInst.IPAddress, config)
	if err != nil {
		return fmt.Errorf("connect to ssh: %w", err)
	}
	defer client.Close()

	return runSSHScript(ctx, client, string(script))
}

func (e *Executor) ProvisionScript() string {
	return ""
}

func authMethod(vmInst vm.Instance) (ssh.AuthMethod, error) {
	if vmInst.PrivateSSHKey == nil && vmInst.Password == "" {
		return nil, fmt.Errorf("one of PrivateSSHKey or Password must be set")
	}

	if vmInst.PrivateSSHKey != nil {
		signer, err := ssh.ParsePrivateKey(vmInst.PrivateSSHKey)
		if err != nil {
			return nil, fmt.Errorf("parsing private key: %w", err)
		}

		return ssh.PublicKeys(signer), nil
	}

	return ssh.Password(vmInst.Password), nil
}

func runSSHScript(ctx context.Context, client *ssh.Client, script string) error {
	// Create a session. It is one session per command.
	session, err := client.NewSession()
	if err != nil {
		return fmt.Errorf("create ssh session: %w", err)
	}
	defer session.Close()

	// both stdout & stderr go to os.Stdout, which is what we display to the user in the build log.
	// os.Stderr is reserved for autoscaler logging
	session.Stdout = os.Stdout
	session.Stderr = os.Stdout

	err = session.Start(script)
	if err != nil {
		return fmt.Errorf("start script: %w", err)
	}

	doneCh := make(chan error, 1)
	go func() {
		doneCh <- session.Wait()
	}()

	select {
	case err = <-doneCh:
		if err != nil {
			return fmt.Errorf("run script: %w", err)
		}
		return nil

	case <-ctx.Done():
		return fmt.Errorf("run script: %w", ctx.Err())
	}
}
