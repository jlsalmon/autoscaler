package config

import (
	"fmt"
	"io/ioutil"

	"github.com/BurntSushi/toml"

	winrm "gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/executors/winrm/config"
	aws "gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/providers/aws/config"
	gcp "gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/providers/gcp/config"
	orka "gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/providers/orka/config"
)

type Global struct {
	Provider string `toml:"Provider"`
	OS       string `toml:"OS"`

	ProviderCache *ProviderCache `toml:"ProviderCache"`

	LogLevel  string `toml:"LogLevel"`
	LogFile   string `toml:"LogFile"`
	LogFormat string `toml:"LogFormat"`

	VMTag string `toml:"VMTag"`

	AWS  aws.Provider  `toml:"AWS"`
	GCP  gcp.Provider  `toml:"GCP"`
	Orka orka.Provider `toml:"Orka"`

	WinRM winrm.Executor `toml:"WinRM"`
}

type ProviderCache struct {
	Enabled   bool   `toml:"Enabled"`
	Directory string `toml:"Directory"`
}

func LoadFromFile(file string) (Global, error) {
	data, err := ioutil.ReadFile(file)
	if err != nil {
		return Global{}, fmt.Errorf("couldn't read configuration file %q: %w", file, err)
	}

	var cfg Global

	err = toml.Unmarshal(data, &cfg)
	if err != nil {
		return Global{}, fmt.Errorf("couldn't parse TOML content of the configuration file: %w", err)
	}

	return cfg, nil
}
