package orka

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"mime"
	"net"
	"net/http"
	"net/url"
	"path"

	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/internal/backoff"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/internal/logging"
)

const (
	endpointStatus = "status"
	endpointCreate = "create"
	endpointDeploy = "deploy"
	endpointPurge  = "purge"
	endpointDelete = "delete"
)

// Client is the interface that an Orka client must implement
type Client interface {
	GetSSHEndpoint(ctx context.Context, name string) (net.Addr, error)
	CreateVM(ctx context.Context, name string, baseImage string, cores int) error
	DeployVM(ctx context.Context, name string) (*Instance, error)
	PurgeVM(ctx context.Context, name string) error
	DeleteVM(ctx context.Context, id string) error
}

// client implements an HTTP Orka API client
// API documentation: https://documenter.getpostman.com/view/6574930/S1ETRGzt?version=latest#intro
type client struct {
	baseURL         url.URL
	token           string
	client          *http.Client
	backoffSettings *backoff.Settings

	logger logging.Logger
}

// New returns a new Orka client
func New(baseURL *url.URL, token string, config *Config) Client {
	url := *baseURL
	url.Fragment = ""

	return &client{
		baseURL:         url,
		token:           token,
		client:          config.Client,
		backoffSettings: config.BackoffSettings,
		logger:          config.Logger.WithField("base_url", url.String()),
	}
}

func getVMEndpointRelURL(parts ...string) string {
	return fmt.Sprintf("resources/vm/%s", path.Join(parts...))
}

// sendHTTPRequestWithRetries performs HTTP requests leveraging sendHTTPRequest
// and the backoff package.
func (c *client) sendHTTPRequestWithRetries(
	ctx context.Context,
	method, endpoint string,
	reqBody interface{},
	respBody interface{},
) error {
	var err error

	if c.backoffSettings == nil {
		return c.sendHTTPRequest(ctx, method, endpoint, reqBody, respBody)
	}

	errBackoff := backoff.Loop(*c.backoffSettings, c.logger, func(log logging.Logger) (bool, error) {
		err = c.sendHTTPRequest(ctx, method, endpoint, reqBody, respBody)
		if err == nil {
			return true, nil
		}

		var urlErr *url.Error
		var httpError *HTTPError
		var shouldBreak bool
		switch {
		case errors.As(err, &urlErr):
			shouldBreak = !urlErr.Timeout()
		case errors.As(err, &httpError):
			shouldBreak = httpError.StatusCode < 500
		default:
			shouldBreak = true
		}

		return shouldBreak, err
	})
	if errBackoff == backoff.ErrMaximumBackOffTimeElapsedExceeded {
		return fmt.Errorf("failure sending request to Orka API: %s /%s: %w", method, endpoint, errBackoff)
	}
	return err
}

// sendHTTPRequest is the workhorse that specializes in performing the HTTP communication
// with the Orka server.
// It marshals and sends the reqBody object in the request, and receives and unmarshals
// the response body into respBody.
func (c *client) sendHTTPRequest(
	ctx context.Context,
	method, endpoint string,
	reqBody interface{},
	respBody interface{},
) error {
	log := c.logger.
		WithField("method", method).
		WithField("endpoint", endpoint)

	log.Debug("Sending Orka API request")

	req, err := c.createRequest(ctx, method, endpoint, reqBody)
	if err != nil {
		log.WithError(err).Error("Failed to create request")
		return fmt.Errorf("creating HTTP request: %w", err)
	}

	resp, err := c.client.Do(req)
	if err != nil {
		log.WithError(err).Error("Failed to send request")
		return fmt.Errorf("sending request to Orka API: %w", err)
	}
	defer resp.Body.Close()

	err = handleResponse(resp, endpoint, respBody, log)
	if err != nil {
		log.WithError(err).Error("Request failed")
		return err
	}

	log.Debug("Request successful")

	return nil
}

func (c *client) createRequest(
	ctx context.Context,
	method string,
	endpoint string,
	reqBody interface{},
) (*http.Request, error) {
	var bodyReader io.Reader
	if reqBody != nil {
		b, err := json.Marshal(reqBody)
		if err != nil {
			return nil, fmt.Errorf("encoding request body: %w", err)
		}

		bodyReader = bytes.NewReader(b)
	}

	requestURL := c.baseURL
	requestURL.Path = endpoint
	req, err := http.NewRequestWithContext(ctx, method, requestURL.String(), bodyReader)
	if err != nil {
		return nil, fmt.Errorf("creating %s request: %w", endpoint, err)
	}

	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", c.token))
	req.Header.Set("Content-Type", "application/json; charset=utf-8")

	return req, nil
}

func handleResponse(resp *http.Response, endpoint string, respBody interface{}, log logging.Logger) error {
	log.WithField("status", resp.Status).Debug("Orka API response received")

	respBodyBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return fmt.Errorf("reading %s response body: %w", endpoint, err)
	}

	if resp.StatusCode >= 400 {
		contentType := resp.Header.Get("Content-Type")
		mediaType, _, err := mime.ParseMediaType(contentType)
		if err != nil {
			return fmt.Errorf("parsing %s Content-Type %q: %w", endpoint, contentType, err)
		}
		if mediaType != "application/json" {
			return &HTTPError{
				Endpoint:   endpoint,
				StatusCode: resp.StatusCode,
				Message:    string(respBodyBytes),
			}
		}

		// An unexpected error was returned, read the error messages from the response
		var rb respBaseBody
		err = json.Unmarshal(respBodyBytes, &rb)
		if err != nil {
			return fmt.Errorf("unmarshalling error JSON response: %s, %w", endpoint, err)
		}

		return &HTTPError{
			Endpoint:   endpoint,
			StatusCode: resp.StatusCode,
			Message:    rb.Message,
			Errors:     rb.Errors.toStringsSlice(),
		}
	}

	if respBody != nil {
		// Unmarshal into respBody so that it is made available to the caller
		err = json.Unmarshal(respBodyBytes, respBody)
		if err != nil {
			return fmt.Errorf("unmarshalling JSON response: %s, %w", endpoint, err)
		}
	}

	return nil
}
