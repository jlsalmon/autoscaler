package orka

import (
	"context"
	"net/http"
)

type purgeVMReqBody struct {
	VMName string `json:"orka_vm_name"`
}

func (c *client) PurgeVM(ctx context.Context, name string) error {
	endpoint := getVMEndpointRelURL(endpointPurge)
	reqBody := purgeVMReqBody{
		VMName: name,
	}
	return c.sendHTTPRequestWithRetries(ctx, http.MethodDelete, endpoint, reqBody, nil)
}
