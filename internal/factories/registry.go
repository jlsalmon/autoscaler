package factories

import (
	"fmt"
)

type ErrNotRegistered struct {
	factoryType string
	name        string
}

func NewErrNotRegistered(factoryType string, name string) *ErrNotRegistered {
	return &ErrNotRegistered{
		factoryType: factoryType,
		name:        name,
	}
}

func (e *ErrNotRegistered) Error() string {
	return fmt.Sprintf("%s %q is not registered", e.factoryType, e.name)
}

type ErrAlreadyRegistered struct {
	factoryType string
	name        string
}

func NewErrAlreadyRegistered(factoryType string, name string) *ErrAlreadyRegistered {
	return &ErrAlreadyRegistered{
		factoryType: factoryType,
		name:        name,
	}
}

func (e *ErrAlreadyRegistered) Error() string {
	return fmt.Sprintf("%s %q is already registered", e.factoryType, e.name)
}

type Registry struct {
	factoryType string
	factories   map[string]interface{}
}

func NewRegistry(factoryType string) *Registry {
	return &Registry{
		factoryType: factoryType,
		factories:   make(map[string]interface{}),
	}
}

func (r *Registry) MustRegister(name string, factory interface{}) {
	_, exists := r.factories[name]
	if exists {
		panic(NewErrAlreadyRegistered(r.factoryType, name))
	}

	r.factories[name] = factory
}

func (r *Registry) Get(name string) (interface{}, error) {
	factory, exists := r.factories[name]
	if !exists {
		return nil, NewErrNotRegistered(r.factoryType, name)
	}

	return factory, nil
}

func (r *Registry) IsRegistered(name string) bool {
	_, ok := r.factories[name]

	return ok
}
