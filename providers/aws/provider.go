package aws

import (
	"context"
	"crypto/sha256"
	"encoding/base64"
	"errors"
	"fmt"
	"github.com/aws/smithy-go"
	globalConfig "gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/config"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/executors"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/internal/logging"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/providers"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/providers/aws/config"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/vm"
	"os"
	"strings"
	"time"

	awsv2 "github.com/aws/aws-sdk-go-v2/aws"
	configv2 "github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/service/ec2"
	"github.com/aws/aws-sdk-go-v2/service/ec2/types"
	"github.com/aws/aws-sdk-go-v2/service/secretsmanager"
)

// TODO:
//  - Allow configuring instance type from .gitlab-ci.yml
//  - Allow configuring docker image from .gitlab-ci.yml
//  - Make sure cleanup happens when build is cancelled

const (
	Name      = "aws"
	osWindows = "windows"

	spotInstanceRequestNotFoundCode = "InvalidSpotInstanceRequestID.NotFound"
)

var (
	errorUnprocessableResponse = errors.New("unexpected AWS API response")
)

type osProvisioner func(
	ctx context.Context,
	logger logging.Logger,
	vmInst *vm.Instance,
	instance *types.Instance,
	e executors.Executor,
) error

type Provider struct {
	config config.Provider
	logger logging.Logger

	ec2            *ec2.Client
	secretsmanager *secretsmanager.Client

	vmTag         string
	osProvisioner osProvisioner
}

func New(cfg globalConfig.Global, logger logging.Logger) (providers.Provider, error) {
	p := &Provider{
		vmTag:  cfg.VMTag,
		config: cfg.AWS,
		logger: logger,
	}

	osProvisioners := map[string]osProvisioner{
		osWindows: p.provisionWindows,
	}

	provisioner, ok := osProvisioners[cfg.OS]
	if !ok {
		return nil, fmt.Errorf("unsupported OS %q for provisioning", cfg.OS)
	}

	p.osProvisioner = provisioner

	return p, nil
}

func (p *Provider) VMName(runnerData vm.NameRunnerData) string {
	url := fmt.Sprintf("%s/%d/%d", runnerData.ProjectURL, runnerData.PipelineID, runnerData.JobID)
	hash := fmt.Sprintf("%x", sha256.Sum256([]byte(url)))

	parts := []string{
		"runner",
		runnerData.RunnerShortToken,
		p.vmTag,
		hash[:20],
	}

	return strings.ToLower(strings.Join(parts, "-"))
}

func (p *Provider) VMNameForRunner(runnerData vm.NameRunnerData) string {
	return p.VMName(runnerData)
}

func (p *Provider) Get(ctx context.Context, name string) (vm.Instance, error) {
	vmInst := vm.Instance{
		Name: name,
		AWS:  config.Instance{},
	}

	ec2client, err := p.getEc2Client(ctx)
	if err != nil {
		return vm.Instance{}, fmt.Errorf("couldn't get the instance details: %w", err)
	}

	input := &ec2.DescribeInstancesInput{
		Filters: []types.Filter{
			{
				Name:   awsv2.String("tag:Name"),
				Values: []string{name},
			},
		},
	}

	result, err := ec2client.DescribeInstances(ctx, input)
	if err != nil {
		return vm.Instance{}, fmt.Errorf("couldn't get the instance details: %w", err)
	}

	var instance = types.Instance{}
	for _, reservation := range result.Reservations {
		for _, inst := range reservation.Instances {
			if inst.State.Name == "running" && inst.PublicIpAddress != nil {
				instance = inst
			}
		}
	}

	vmInst.IPAddress = *instance.PrivateIpAddress
	vmInst.AWS.InstanceID = *instance.InstanceId

	err = p.loadCredentialsFromSecretsManager(ctx, &vmInst)
	if err != nil {
		p.logger.WithError(err).Warn("couldn't load the credentials")
	}

	return vmInst, nil
}

func (p *Provider) Create(ctx context.Context, e executors.Executor, vmInst *vm.Instance) error {
	log := p.logger.WithField("vm-name", vmInst.Name)
	log.Info("Creating Virtual Machine...")

	ec2client, err := p.getEc2Client(ctx)
	if err != nil {
		return err
	}

	keypair, err := p.createKeyPair(ctx, ec2client, vmInst)
	if err != nil {
		return err
	}

	vmInst.AWS.KeyPairId = *keypair.KeyPairId
	vmInst.AWS.KeyMaterial = *keypair.KeyMaterial

	// Inject environment variables for use inside userdata
	env := "\n"
	for _, key := range p.config.UserDataEnvironmentKeys {
		value := os.Getenv(key)
		env += fmt.Sprintf("$env:%s = '%s'\n", key, value)
	}

	userData := base64.StdEncoding.EncodeToString([]byte(fmt.Sprintf(`
<powershell>
try {
	%s
	%s
	%s
} 
catch {
  Write-Error $_.Exception
  exit 1
}
</powershell>
<persist>true</persist>`,
		env, e.ProvisionScript(), p.config.UserData)))

	instanceProfileSpec := &types.IamInstanceProfileSpecification{
		Name: awsv2.String(p.config.GetInstanceProfile()),
	}

	bdms := []types.BlockDeviceMapping{{
		DeviceName: awsv2.String(p.config.GetDeviceName()),
		Ebs: &types.EbsBlockDevice{
			VolumeSize:          awsv2.Int32(p.config.GetRootSize()),
			VolumeType:          p.config.GetVolumeType(),
			DeleteOnTermination: awsv2.Bool(true),
		},
	}}

	netSpecs := []types.InstanceNetworkInterfaceSpecification{{
		DeviceIndex:              awsv2.Int32(0),
		Groups:                   []string{p.config.GetSecurityGroup()},
		SubnetId:                 awsv2.String(p.config.GetSubnetId()),
		AssociatePublicIpAddress: awsv2.Bool(!p.config.PrivateAddressOnly),
	}}

	tags := []types.Tag{{
		Key:   awsv2.String("Name"),
		Value: &vmInst.Name,
	}}

	var instance *types.Instance

	if p.config.RequestSpotInstance {
		log.Info("Requesting spot instance")

		req := ec2.RequestSpotInstancesInput{
			LaunchSpecification: &types.RequestSpotLaunchSpecification{
				ImageId:             awsv2.String(p.config.GetImageId()),
				KeyName:             keypair.KeyName,
				InstanceType:        p.config.GetInstanceType(),
				NetworkInterfaces:   netSpecs,
				IamInstanceProfile:  instanceProfileSpec,
				BlockDeviceMappings: bdms,
				UserData:            &userData,
				Placement: &types.SpotPlacement{
					AvailabilityZone: awsv2.String(p.config.GetRegionZone()),
				},
			},
			InstanceCount: awsv2.Int32(1),
			SpotPrice:     awsv2.String(p.config.GetSpotPrice()),
			TagSpecifications: []types.TagSpecification{{
				ResourceType: "spot-instances-request",
				Tags:         tags,
			}},
		}

		spotInstanceRequest, err := ec2client.RequestSpotInstances(ctx, &req)
		if err != nil {
			return fmt.Errorf("error requesting spot instance: %v", err)
		}
		if spotInstanceRequest == nil || len((*spotInstanceRequest).SpotInstanceRequests) < 1 {
			return fmt.Errorf("error requesting spot instance: %v", errorUnprocessableResponse)
		}
		vmInst.AWS.SpotInstanceRequestId = *spotInstanceRequest.SpotInstanceRequests[0].SpotInstanceRequestId

		log.Info("Waiting for spot instance...")
		for i := 0; i < 3; i++ {
			// AWS eventual consistency means we could not have SpotInstanceRequest ready yet
			waiter := ec2.NewSpotInstanceRequestFulfilledWaiter(ec2client)
			params := &ec2.DescribeSpotInstanceRequestsInput{
				SpotInstanceRequestIds: []string{vmInst.AWS.SpotInstanceRequestId},
			}
			err := waiter.Wait(ctx, params, 5*time.Minute)

			if err != nil {
				var apiErr smithy.APIError
				if errors.As(err, &apiErr) {
					if apiErr.ErrorCode() == spotInstanceRequestNotFoundCode {
						time.Sleep(5 * time.Second)
						continue
					}
				}

				return fmt.Errorf("error fulfilling spot request: %v", err)
			}
			break
		}
		log.Infof("Created spot instance request %v", vmInst.AWS.SpotInstanceRequestId)
		// resolve instance id
		for i := 0; i < 3; i++ {
			// Even though the waiter succeeded, eventual consistency means we could
			// get a describe output that does not include this information. Try a
			// few times just in case
			var resolvedSpotInstance *ec2.DescribeSpotInstanceRequestsOutput
			resolvedSpotInstance, err = ec2client.DescribeSpotInstanceRequests(ctx, &ec2.DescribeSpotInstanceRequestsInput{
				SpotInstanceRequestIds: []string{vmInst.AWS.SpotInstanceRequestId},
			})
			if err != nil {
				// Unexpected; no need to retry
				return fmt.Errorf("error describing previously made spot instance request: %v", err)
			}
			if resolvedSpotInstance == nil || len((*resolvedSpotInstance).SpotInstanceRequests) < 1 {
				return fmt.Errorf("error describing spot instance: %v", errorUnprocessableResponse)
			}

			maybeInstanceId := resolvedSpotInstance.SpotInstanceRequests[0].InstanceId
			if maybeInstanceId != nil {
				var instances *ec2.DescribeInstancesOutput
				instances, err = ec2client.DescribeInstances(ctx, &ec2.DescribeInstancesInput{
					InstanceIds: []string{*maybeInstanceId},
				})
				if err != nil {
					// Retry if we get an id from spot instance but EC2 doesn't
					// recognize it yet; see above, eventual consistency possible
					continue
				}
				instance = &instances.Reservations[0].Instances[0]
				err = nil
				break
			}
			time.Sleep(5 * time.Second)
		}

		if err != nil {
			return fmt.Errorf("error resolving spot instance to real instance: %v", err)
		}

		_, err = ec2client.CreateTags(ctx, &ec2.CreateTagsInput{
			Resources: []string{
				*instance.InstanceId,
				*instance.BlockDeviceMappings[0].Ebs.VolumeId,
				*instance.NetworkInterfaces[0].NetworkInterfaceId,
			},
			Tags: tags,
		})

	} else {
		tagSpecs := []types.TagSpecification{{
			ResourceType: "instance",
			Tags:         tags,
		}, {
			ResourceType: "volume",
			Tags:         tags,
		}}

		input := &ec2.RunInstancesInput{
			ImageId:             awsv2.String(p.config.GetImageId()),
			InstanceType:        p.config.GetInstanceType(),
			MinCount:            awsv2.Int32(1),
			MaxCount:            awsv2.Int32(1),
			KeyName:             keypair.KeyName,
			NetworkInterfaces:   netSpecs,
			UserData:            &userData,
			BlockDeviceMappings: bdms,
			IamInstanceProfile:  instanceProfileSpec,
			TagSpecifications:   tagSpecs,
			Placement: &types.Placement{
				AvailabilityZone: awsv2.String(p.config.GetRegionZone()),
			},
		}

		result, err := ec2client.RunInstances(ctx, input)
		if err != nil {
			fmt.Printf("Failed to create EC2 instance: %s", err)

			instances, err2 := ec2client.DescribeInstances(ctx, &ec2.DescribeInstancesInput{
				Filters: []types.Filter{{
					Name:   awsv2.String("tag:Name"),
					Values: []string{vmInst.Name},
				}},
			})
			if err2 != nil {
				return fmt.Errorf("couldn't describe failed instance %s: %s: %w", vmInst.Name, err2, err)
			}

			if len(instances.Reservations) == 1 {
				reservation := instances.Reservations[0]
				if len(reservation.Instances) == 1 {
					instance := reservation.Instances[0]

					_, err2 := ec2client.TerminateInstances(ctx, &ec2.TerminateInstancesInput{
						InstanceIds: []string{*instance.InstanceId},
					})
					if err2 != nil {
						return fmt.Errorf("couldn't terminate failed instance %s: %s: %w", vmInst.Name, err2, err)
					}
				}
			}

			return err
		}

		instance = &result.Instances[0]
	}

	fmt.Printf("Created %s instance %s (%s)\n", instance.InstanceType, vmInst.Name, *instance.InstanceId)

	vmInst.AWS.InstanceID = *instance.InstanceId
	vmInst.IPAddress = *instance.PrivateIpAddress

	if p.osProvisioner != nil {
		err = p.osProvisioner(ctx, log, vmInst, instance, e)
		if err != nil {
			return fmt.Errorf("couldn't provision the OS: %w", err)
		}
	}

	err = p.saveCredentialsInSecretsManager(ctx, vmInst)
	if err != nil {
		return fmt.Errorf("couldn't save username and password in secretsmanager: %w", err)
	}

	log.Info("Virtual Machine created")

	return nil
}

func (p *Provider) createKeyPair(ctx context.Context, ec2client *ec2.Client, vmInst *vm.Instance) (*ec2.CreateKeyPairOutput, error) {
	keypairs, err := ec2client.DescribeKeyPairs(ctx, &ec2.DescribeKeyPairsInput{
		Filters: []types.Filter{{
			Name:   awsv2.String("key-name"),
			Values: []string{vmInst.Name},
		}},
	})
	if err != nil {
		return nil, err
	}

	if len(keypairs.KeyPairs) == 1 {
		_, err := ec2client.DeleteKeyPair(ctx, &ec2.DeleteKeyPairInput{
			KeyName: &vmInst.Name,
		})
		if err != nil {
			return nil, err
		}
	}

	keypair, err := ec2client.CreateKeyPair(ctx, &ec2.CreateKeyPairInput{
		KeyName: &vmInst.Name,
	})
	if err != nil {
		return nil, err
	}

	return keypair, nil
}

func (p *Provider) Delete(ctx context.Context, vmInst vm.Instance) error {
	log := p.logger.WithField("vm-name", vmInst.Name)
	log.Info("Deleting Virtual Machine...")

	ec2client, err := p.getEc2Client(ctx)
	if err != nil {
		return err
	}

	secretsmanagerclient, err := p.getSecretsManagerClient(ctx)
	if err != nil {
		return err
	}

	if len(vmInst.AWS.InstanceID) > 0 {
		_, err = ec2client.TerminateInstances(ctx, &ec2.TerminateInstancesInput{
			InstanceIds: []string{vmInst.AWS.InstanceID},
		})
		if err != nil {
			return err
		}

		log.Infof("Terminated instance: %s", vmInst.AWS.InstanceID)
	}

	if len(vmInst.AWS.KeyPairId) > 0 {
		_, err = ec2client.DeleteKeyPair(ctx, &ec2.DeleteKeyPairInput{
			KeyPairId: &vmInst.AWS.KeyPairId,
		})
		if err != nil {
			return err
		}

		log.Infof("Deleted keypair: %s", vmInst.AWS.KeyPairId)
	}

	if len(vmInst.AWS.SecretName) > 0 {
		_, err = secretsmanagerclient.DeleteSecret(ctx, &secretsmanager.DeleteSecretInput{
			SecretId:                   &vmInst.AWS.SecretName,
			ForceDeleteWithoutRecovery: true,
			RecoveryWindowInDays:       0,
		})
		if err != nil {
			return err
		}

		log.Infof("Deleted secret: %s", vmInst.AWS.SecretName)
	}

	return nil
}

func (p *Provider) saveCredentialsInSecretsManager(ctx context.Context, vmInst *vm.Instance) error {
	log := p.logger.WithField("vm-name", vmInst.Name)
	log.Info("Saving credentials in Secrets Manager...")

	secretsmanagerclient, err := p.getSecretsManagerClient(ctx)
	if err != nil {
		return err
	}

	secretName := fmt.Sprintf("gitlab/ci/%s", vmInst.Name)
	description := "Created by GitLab CI autoscaler executor"

	secret, err := secretsmanagerclient.CreateSecret(ctx, &secretsmanager.CreateSecretInput{
		Name:         &secretName,
		Description:  &description,
		SecretString: &vmInst.Password,
	})
	if err != nil {
		return err
	}

	vmInst.AWS.SecretName = *secret.Name

	log.Infof("Credentials saved in Secrets Manager (%s)", vmInst.AWS.SecretName)
	return nil
}

func (p *Provider) loadCredentialsFromSecretsManager(ctx context.Context, vmInst *vm.Instance) error {
	secretsmanagerclient, err := p.getSecretsManagerClient(ctx)
	if err != nil {
		return err
	}

	secret, err := secretsmanagerclient.GetSecretValue(ctx, &secretsmanager.GetSecretValueInput{
		SecretId: &vmInst.AWS.SecretName,
	})
	if err != nil {
		return err
	}

	vmInst.Username = p.config.Username
	vmInst.Password = *secret.SecretString

	return nil
}

func (p *Provider) getEc2Client(ctx context.Context) (*ec2.Client, error) {
	if p.ec2 != nil {
		return p.ec2, nil
	}

	cfg, err := p.loadConfig(ctx)
	if err != nil {
		return nil, err
	}

	ec2Client := ec2.NewFromConfig(cfg)
	p.ec2 = ec2Client

	return ec2Client, nil
}

func (p *Provider) getSecretsManagerClient(ctx context.Context) (*secretsmanager.Client, error) {
	if p.secretsmanager != nil {
		return p.secretsmanager, nil
	}

	cfg, err := p.loadConfig(ctx)
	if err != nil {
		return nil, err
	}

	secretsmanagerclient := secretsmanager.NewFromConfig(cfg)
	p.secretsmanager = secretsmanagerclient

	return secretsmanagerclient, nil
}

func (p *Provider) loadConfig(ctx context.Context) (awsv2.Config, error) {
	cfg, err := configv2.LoadDefaultConfig(ctx, configv2.WithRegion(p.config.Region))
	if err != nil {
		return awsv2.Config{}, fmt.Errorf("couldn't create AWS EC2 client: %w", err)
	}

	return cfg, nil
}
