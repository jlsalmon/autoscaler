package config

import (
	"fmt"
	"github.com/aws/aws-sdk-go-v2/service/ec2/types"
	"os"
	"strconv"
	"strings"
)

const (
	defaultRegion       = "us-east-1"
	defaultImageId      = "ami-058faf96ea153ed9f"
	defaultInstanceType = "t2.micro"
	defaultDeviceName   = "/dev/sda1"
	defaultRootSize     = 16
	defaultVolumeType   = "gp2"
	defaultZone         = "a"
	defaultSpotPrice    = "0.50"

	envRegion          = "AWS_REGION"
	envImageId         = "AWS_IMAGE_ID"
	envInstanceType    = "AWS_INSTANCE_TYPE"
	envDeviceName      = "AWS_DEVICE_NAME"
	envRootSize        = "AWS_ROOT_SIZE"
	envVolumeType      = "AWS_VOLUME_TYPE"
	envVpcId           = "AWS_VPC_ID"
	envSubnetId        = "AWS_SUBNET_ID"
	envSecurityGroup   = "AWS_SECURITY_GROUP"
	envZone            = "AWS_ZONE"
	envInstanceProfile = "AWS_INSTANCE_PROFILE"
	envSpotPrice       = "AWS_SPOT_PRICE"
)

type Provider struct {
	Region          string             `toml:"Region"`
	ImageId         string             `toml:"ImageId"`
	InstanceType    types.InstanceType `toml:"InstanceType"`
	VpcId           string             `toml:"VpcId"`
	SubnetId        string             `toml:"SubnetId"`
	Zone            string             `toml:"Zone"`
	SecurityGroup   string             `toml:"SecurityGroup"`
	InstanceProfile string             `toml:"InstanceProfile"`

	DeviceName string           `toml:"DeviceName"`
	VolumeType types.VolumeType `toml:"VolumeType"`
	RootSize   int32            `toml:"RootSize"`

	UserData                string   `toml:"UserData"`
	UserDataEnvironmentKeys []string `toml:"UserDataEnvironmentKeys"`

	// TODO: implement these
	PrivateAddressOnly  bool   `toml:"PrivateAddressOnly"`
	RequestSpotInstance bool   `toml:"RequestSpotInstance"`
	SpotPrice           string `toml:"SpotPrice"`

	// TODO: implement this
	Tags map[string]string `toml:"Tags"`

	Username string `toml:"Username"`
}

func (p *Provider) GetRegion() string {
	return getOptionString(envRegion, p.Region, defaultRegion)
}

func (p *Provider) GetRegionZone() string {
	return p.GetRegion() + p.GetZone()
}

func (p *Provider) GetImageId() string {
	return getOptionString(envImageId, p.ImageId, defaultImageId)
}

func (p *Provider) GetInstanceType() types.InstanceType {
	return types.InstanceType(getOptionString(envInstanceType, string(p.InstanceType), defaultInstanceType))
}

func (p *Provider) GetVpcId() string {
	return getOptionString(envVpcId, p.VpcId, "")
}

func (p *Provider) GetSubnetId() string {
	return getOptionString(envSubnetId, p.SubnetId, "")
}

func (p *Provider) GetZone() string {
	return getOptionString(envZone, p.Zone, defaultZone)
}

func (p *Provider) GetSecurityGroup() string {
	return getOptionString(envSecurityGroup, p.SecurityGroup, "")
}

func (p *Provider) GetInstanceProfile() string {
	return getOptionString(envInstanceProfile, p.InstanceProfile, "")
}

func (p *Provider) GetDeviceName() string {
	return getOptionString(envDeviceName, p.DeviceName, defaultDeviceName)
}

func (p *Provider) GetVolumeType() types.VolumeType {
	return types.VolumeType(getOptionString(envVolumeType, string(p.VolumeType), defaultVolumeType))
}

func (p *Provider) GetRootSize() int32 {
	rootSize, err := getOptionInt(envRootSize, p.RootSize, defaultRootSize)
	if err != nil {
		fmt.Printf("ERROR: could not parse RootSize, using default value %d (error: %s)\n", defaultRootSize, err)
		return defaultRootSize
	}
	return rootSize
}

func (p *Provider) GetSpotPrice() string {
	return getOptionString(envSpotPrice, p.SpotPrice, defaultSpotPrice)
}

func getOptionString(envKey, fileValue, fallback string) string {
	if value, ok := os.LookupEnv(envKey); ok {
		return value
	}
	if value, ok := os.LookupEnv("CUSTOM_ENV_" + strings.ToLower(envKey)); ok {
		return value
	}
	if fileValue != "" {
		return fileValue
	}
	return fallback
}

func getOptionInt(envKey string, fileValue, fallback int32) (int32, error) {
	if value, ok := os.LookupEnv(envKey); ok {
		v, err := strconv.ParseInt(value, 10, 32)
		if err != nil {
			return -1, err
		}
		return int32(v), nil
	}
	if value, ok := os.LookupEnv("CUSTOM_ENV_" + strings.ToLower(envKey)); ok {
		v, err := strconv.ParseInt(value, 10, 32)
		if err != nil {
			return -1, err
		}
		return int32(v), nil
	}
	if fileValue != 0 {
		return fileValue, nil
	}
	return fallback, nil
}
