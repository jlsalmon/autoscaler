package config

type Instance struct {
	InstanceID            string
	KeyPairId             string
	KeyMaterial           string
	SecretName            string
	SpotInstanceRequestId string
}
