package aws

import (
	"bufio"
	"bytes"
	"context"
	"crypto/rsa"
	"crypto/x509"
	"encoding/base64"
	"encoding/pem"
	"fmt"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/executors"
	"strings"
	"time"

	"github.com/aws/aws-sdk-go-v2/service/ec2"
	"github.com/aws/aws-sdk-go-v2/service/ec2/types"

	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/internal/backoff"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/internal/logging"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/vm"
)

func (p *Provider) provisionWindows(
	ctx context.Context,
	logger logging.Logger,
	vmInst *vm.Instance,
	instance *types.Instance,
	e executors.Executor,
) error {
	log := logger.WithField("os", osWindows)
	log.Info("Provisioning Windows OS...")

	fmt.Println("Provisioning Windows instance...")

	password, err := p.getWindowsPassword(ctx, log, vmInst, instance)
	if err != nil {
		return fmt.Errorf("couldn't get Windows password for the %q instance: %w", vmInst.Name, err)
	}

	logger.Info("Got Windows password")

	vmInst.Username = p.config.Username
	vmInst.Password = password

	// FIXME: for some reason when Fast Launch is enabled on the AMI, we can get
	//   the password but the console log is empty.
	err = p.waitForWindowsReadiness(ctx, log, instance, vmInst)
	if err != nil {
		return fmt.Errorf("couldn't ensure that Windows is ready for the %q instance: %w", vmInst.Name, err)
	}

	err = p.waitForUserDataCompletion(ctx, log, vmInst, e)
	if err != nil {
		return fmt.Errorf("couldn't ensure that Windows is ready for the %q instance: %w", vmInst.Name, err)
	}

	log.Info("Windows OS provisioned")

	return nil
}

func (p *Provider) getWindowsPassword(
	ctx context.Context,
	logger logging.Logger,
	vmInst *vm.Instance,
	instance *types.Instance,
) (string, error) {
	logger.Info("Getting Windows password...")

	ec2client, err := p.getEc2Client(ctx)
	if err != nil {
		return "", err
	}

	b := backoff.Settings{
		InitialInterval:     15,
		RandomizationFactor: 0.1,
		Multiplier:          1.25,
		MaxInterval:         30,
		MaxElapsedTime:      600,
	}
	var output = &ec2.GetPasswordDataOutput{}

	err = backoff.Loop(b, logger, func(logger logging.Logger) (bool, error) {
		logger.Info("Reading Virtual Machine password...")

		output, err = ec2client.GetPasswordData(ctx, &ec2.GetPasswordDataInput{
			InstanceId: instance.InstanceId,
		})

		if err == nil {
			if len(*output.PasswordData) == 0 {
				return false, nil
			}

			return true, nil
		}

		logger.WithError(err).Debug("couldn't get password")
		return false, nil
	})

	if err != nil {
		return "", fmt.Errorf("couldn't read encrypted password: %w", err)
	}

	logger.Info("Decoding and decrypting Virtual Machine password...")

	encryptedPassword, err := base64.StdEncoding.DecodeString(*output.PasswordData)
	if err != nil {
		return "", fmt.Errorf("couldn't decode password: %w", err)
	}

	block, _ := pem.Decode([]byte(vmInst.AWS.KeyMaterial))

	key, err := x509.ParsePKCS1PrivateKey(block.Bytes)
	if err != nil {
		return "", fmt.Errorf("couldn't parse private key: %w", err)
	}

	password, err := rsa.DecryptPKCS1v15(nil, key, encryptedPassword)
	if err != nil {
		return "", fmt.Errorf("couldn't decrypt password: %w", err)
	}

	return string(password), nil
}

func (p *Provider) waitForWindowsReadiness(
	ctx context.Context,
	logger logging.Logger,
	instance *types.Instance,
	vmInst *vm.Instance,
) error {
	ec2client, err := p.getEc2Client(ctx)
	if err != nil {
		return err
	}

	b := backoff.Settings{
		InitialInterval:     30,
		RandomizationFactor: 0.1,
		Multiplier:          1.25,
		MaxInterval:         30,
		MaxElapsedTime:      600,
	}
	err = backoff.Loop(b, logger, func(logger logging.Logger) (bool, error) {
		logger.Info("Checking Windows Virtual Machine readiness")

		output, err := ec2client.GetConsoleOutput(ctx, &ec2.GetConsoleOutputInput{
			InstanceId: instance.InstanceId,
		})
		if err != nil {
			return false, err
		}

		if output.Output == nil {
			return false, nil
		}

		log, err := base64.StdEncoding.DecodeString(*output.Output)

		if err == nil && readinessDetected(string(log)) {
			logger.Debug("Windows Virtual Machine is ready")
			return true, nil
		}

		return false, err
	})

	if err != nil {
		return fmt.Errorf("Windows Virtual Machine %q is not ready: %w", vmInst.Name, err)
	}

	return nil
}

func readinessDetected(content string) bool {
	return strings.Contains(content, "Windows is Ready to use")
}

func (p *Provider) waitForUserDataCompletion(
	ctx context.Context,
	logger logging.Logger,
	vmInst *vm.Instance,
	e executors.Executor,
) error {
	b := backoff.Settings{
		InitialInterval:     0,
		RandomizationFactor: 0.1,
		Multiplier:          1.25,
		MaxInterval:         10,
		MaxElapsedTime:      300,
	}
	err := backoff.Loop(b, logger, func(logger logging.Logger) (bool, error) {
		logger.Info("Checking user data execution")

		script := "Get-Content c:\\ProgramData\\Amazon\\EC2-Windows\\Launch\\Log\\UserdataExecution.log"
		var stdout bytes.Buffer
		var stderr bytes.Buffer
		errored := false

		err := e.ExecuteWithWriters(ctx, *vmInst, []byte(script), &stdout, &stderr)
		if err != nil {
			return false, err
		}

		// User data might be configured to execute on each boot, so we consider
		// only the most recent execution and assume that it must have completed
		// within the last 60 seconds
		s := bufio.NewScanner(&stdout)
		for s.Scan() {
			line := s.Text()
			if userDataExecutionCompleted(line) {
				timestamp, err := time.Parse("2006/01/02 15:04:05Z", line[0:20])
				if err != nil {
					return false, err
				}

				if time.Now().Sub(timestamp).Seconds() < 60 {
					logger.Infof("User data execution completed at %s", timestamp.String())
					return true, nil
				}
			}

			if userDataExecutionErrored(line) {
				fmt.Printf("WARNING: User data execution contained errors\n")
				errored = true
			}

			if errored {
				fmt.Println(line)
			}
		}

		return false, nil
	})

	if err != nil {
		return fmt.Errorf("user data has not been executed: %w", err)
	}

	return nil
}

func userDataExecutionCompleted(content string) bool {
	return strings.Contains(content, "Userdata execution done")
}

func userDataExecutionErrored(content string) bool {
	return strings.Contains(content, "Message: The errors from user scripts:")
}
