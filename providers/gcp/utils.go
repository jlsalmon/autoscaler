package gcp

import (
	"fmt"
	"regexp"
)

const (
	gcpAPIURL          = "https://www.googleapis.com/compute/v1"
	resourceURIPattern = "%s/projects/%s/%s/%s/%s"

	locationTypeGlobal = "global"
	locationTypeZone   = "zones"
	locationTypeRegion = "regions"

	resourcesDiskType    = "diskTypes"
	resourcesMachineType = "machineTypes"
	resourceNetwork      = "networks"
	resourceSubnetwork   = "subnetworks"
)

var (
	zoneRxp = regexp.MustCompile("^([a-z]+-[a-z0-9]+)-([a-z])$")
)

func globalResourceURI(project string, resourceType string, resource string) string {
	return resourceURI(project, locationTypeGlobal, resourceType, resource)
}

func regionResourceURI(project string, region string, resourceType string, resource string) string {
	return resourceURI(project, locationTypeRegion+"/"+region, resourceType, resource)
}

func zoneResourceURI(project string, zone string, resourceType string, resource string) string {
	return resourceURI(project, locationTypeZone+"/"+zone, resourceType, resource)
}

func resourceURI(project string, location string, resourceType string, resource string) string {
	return fmt.Sprintf(resourceURIPattern, gcpAPIURL, project, location, resourceType, resource)
}

func zoneToRegion(zone string) (string, error) {
	parts := zoneRxp.FindAllStringSubmatch(zone, -1)
	if parts == nil {
		return "", fmt.Errorf("%q is not a valid GCP zone", zone)
	}

	return parts[0][1], nil
}
