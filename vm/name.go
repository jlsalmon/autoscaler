package vm

type NameRunnerData struct {
	RunnerShortToken string
	ProjectURL       string
	PipelineID       int64
	JobID            int64
}
