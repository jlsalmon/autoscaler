package vm

import (
	aws "gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/providers/aws/config"
	gcp "gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/providers/gcp/config"
	orka "gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/providers/orka/config"
)

type Instance struct {
	Name string

	IPAddress string

	// For machine authentication
	Username      string
	Password      string
	PrivateSSHKey []byte

	AWS  aws.Instance
	GCP  gcp.Instance
	Orka orka.Instance
}
